<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kapitaluz
 */

$text_btn_one  = get_field( 'text_btn_one', 'option' );
$link_btn_one  = get_field( 'link_btn_one', 'option' );
$text_btn_two  = get_field( 'text_btn_two', 'option' );
$link_btn_two  = get_field( 'link_btn_two', 'option' );
$color_btn_two = get_field( 'color_btn_2', 'option' );
$color_btn_one = get_field( 'color_btn_1', 'option' );

$termObj = get_queried_object();
if ( in_category( [ 'biznes-akademiya' ], get_the_ID() ) || 'biznes-akademiya' === $termObj->slug ) {
	$logo = 'logo.svg';
} else {
	$logo = 'logo-white.svg';
}
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link
			href="https://fonts.googleapis.com/css2?family=Oranienbaum&amp;family=Orbitron&amp;family=Roboto+Condensed&amp;family=Roboto:wght@400;500;700&amp;family=Raleway:wght@600&amp;display=swap"
			rel="stylesheet">


	<script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>
	<meta name="google-site-verification" content="VoFlYTpgw7L-LpztcGpxyKc6NgskTopITYHT2ew1FF8"/>
	<meta name="yandex-verification" content="d3a4143a9dee7516"/>


	<link rel="apple-touch-icon" href="<?php esc_url( bloginfo( 'url' ) . '/apple-touch-icon.png' ); ?>">
	<meta name="msapplication-TileColor" content="#c33">
	<meta name="msapplication-TileImage" content="<?php esc_url( bloginfo( 'url' ) . '/apple-touch-icon.png' ); ?>">
	<meta name="theme-color" content="#04144f">
	<link rel="manifest" href="<?php esc_url( bloginfo( 'url' ) . '/manifest.json' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<header class="main-header">
	<div class="container">
		<div class="main-header__inner">
			<div class="left-side">
				<div class="btn-wrapper" id="burger-btn">
					<div class="burger-btn"></div>
				</div>
			</div>
			<div class="center">
				<a href="<?php bloginfo( 'url' ); ?>" class="img-wrapper logo">
					<img
							src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/' . $logo ); ?>"
							alt="Site logo">
				</a>
			</div>
			<div class="right-side">
				<?php if ( ! empty( $text_btn_one ) || ! empty( $link_btn_one ) ) : ?>
					<a
							class="button tbc-btn"
							id="btn-one"
							style="background-color: <?php echo $color_btn_one ?? 'inherit'; ?>"
							href="<?php echo esc_url( $link_btn_one ); ?>">
						<?php echo esc_html( $text_btn_one ); ?>
					</a>
				<?php endif; ?>
				<?php if ( ! empty( $text_btn_tow ) || ! empty( $link_btn_two ) ) : ?>
					<a
							class="button tbc-btn"
							id="btn-two"
							style="background-color: <?php echo $color_btn_two ?? 'inherit'; ?>"
							href="<?php echo esc_url( $link_btn_two ); ?>">
						<?php echo esc_html( $text_btn_two ); ?>
					</a>
				<?php endif; ?>
				<a href="https://kapital.uz/category/kapital-tv" class="video-button">
					<h3 class="section-title">Kapital TV</h3>
					<img
							class="play-logo"
							src="
							<?php
							echo esc_url( get_template_directory_uri() . '/assets/img/play-img.png' )
							?>
							">
				</a>
				<button class="search-btn" id="src-button">
					<img
							class="search"
							src="
							<?php
							echo esc_url( get_template_directory_uri() . '/assets/img/search-white.png' )
							?>
							">
				</button>
			</div>
		</div>
	</div>
	<div class="burger-menu" id="nav-menu">
		<?php
		wp_nav_menu(
			[
				'theme_location' => 'menu-1',
				'container'      => '',
			]
		);
		?>
		<form action="/" class="search-field burger-search">
			<input required type="text" placeholder="Поиск..." class="search"/>
			<button type="submit"></button>
		</form>
	</div>
	<div class="burger-menu search-form-wrapper js-search-form-wrapper" id="src-menu">
		<form action="/" class="search-field">
			<input required type="text" name="s" placeholder="Поиск..." class="search"/>
			<button type="submit" class="search-btn"></button>
		</form>
	</div>
</header>
