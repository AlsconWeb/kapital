<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package kapitaluz
 */

get_header();
?>
<main>
	<section class="article">
		<div class="container">
			<div class="main-post__inner">
				<div class="main-post__right">
					<div class="main-post-content">
						<div class="main-post-right">
							<div class="latest-news">
								<div class="container">
									<h3 class="section-title"><?php printf( __( '<b>Результаты запроса:</b> %s', 'twentyfourteen' ), get_search_query() );?></h3>
									<div class="lates-news__inner">
										<div class="big-post-wrapper">
										<?php while (have_posts()) : the_post();?>
											<div class="post">
												<a class="img-wrapper" href="<?php the_permalink(); ?>"> <img src="<?php echo get_the_post_thumbnail_url($post, 'full');?>" alt="<?php the_title(); ?>"> </a>
												<div class="post__content">
													<div class="post-top"><?php echo get_the_category_list(); ?><span class="breaker">/</span> <span class="post-date"><?php echo get_the_date('j F Y'); ?></span></div>
													<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													<p><?= get_the_excerpt(); ?></p>
												</div>
											</div>
											<?php endwhile; ?>
											<a href="#" class="load-more-link"> Показать еще </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="main-post-ads">
					<?php get_sidebar(); ?>
                </div>
			</div>
		</div>
	</section>
</main>
									




<?php
get_footer();
