<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kapitaluz
 */
get_header();
?>
<section class="rubric-section">
	<div class="container">
		<h1 class="section-title">
		<?php $this_category = get_category($cat);?>
		<?php echo $this_category->cat_name; ?>
		</h1>
		<?php if ( is_category() ) : ?> 
		<?php endif; ?>
		<div class="main-post__inner">
			<div class="main-post-content">
				<div class="main-post-right">
					<div class="rubric-posts-wrapper hide-desc" id="loadmoreContent">

						<?php while ( have_posts() ) : the_post(); ?>

						<div class="post">
							<a class="img-wrapper" href="<?php the_permalink(); ?>"> <img src="<?php echo get_the_post_thumbnail_url($post, 'medium'); ?>" alt="<?php the_title(); ?>" /> </a>
							<div class="post__content">
								<div class="post-top"><?php echo get_the_category_list(); ?><span class="breaker">/</span> <span class="post-date"><?php echo get_the_date('j F Y'); ?></span></div>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</div>
						</div>
						<?php endwhile; // end of the loop. ?>
					</div>
					<?php 
						global $wp_query;
						if (  $wp_query->max_num_pages > 1 ) {
							echo '<div class="misha_loadmore">Показать еще</div>'; // you can use <a> as well
						}
				 ?>
				</div>
				<div class="main-post-ads">
					<?php get_sidebar(); ?>
					<div class="popular-posts">
						<h4 class="sidesection-title">Популярное</h4>
						<div class="popular-posts-wrapper">
						<?php $args = array(
										'post_type' => 'post',
										'showposts' => '4',
										'offset' => '1',
										'tax_query' => array(
											array(
													'category' => '',
												),
											),
										);
										
										$the_query = new WP_Query( $args );?>
							
										<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>


							<div class="post">
								<div class="post__content">
									<div class="post-top"><?php echo get_the_category_list(); ?><span class="breaker">/</span> <span class="post-date"><?php echo get_the_date('j F Y'); ?></span></div>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
					<div class="latest-posts">
						<h4 class="sidesection-title">Последние новости</h4>
						<div class="popular-posts-wrapper">
							<?php $args = array(
										'post_type' => 'post',
										'showposts' => '4',
										'offset' => '1',
										'tax_query' => array(
											array(
													'category' => '',
												),
											),
										);
										
										$the_query = new WP_Query( $args );?>
							
										<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
							<div class="post">
								<div class="post__content">
									<div class="post-top"><?php echo get_the_category_list(); ?><span class="breaker">/</span> <span class="post-date"><?php echo get_the_date('j F Y'); ?></span></div>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
                   </div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
