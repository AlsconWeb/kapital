<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package kapitaluz
 */

get_header();
?>
<main>
	<section class="article">
		<div class="container">
			<div class="main-post__inner">
				<div class="main-post__right">
					<div class="main-post-content">
						<div class="main-post-right">
							<h1 class="article-title"><?php wp_title(''); ?></h1>
							<hr>
							<div class="article-content"><?php the_content(); ?></div>
							<p class="error-not-found">404</p>
						</div>
					</div>
				</div>
				<div class="main-post-ads">
				<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</section>
	<div class="mob-banner">
		<div class="container">
			<div class="img-wrapper"><img src="img/banner-mob.png" alt="banner" /></div>
		</div>
	</div>
</main>

<?php
get_footer();