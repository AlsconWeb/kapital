<?php
/* Template Name: Brand page*/

get_header('transparent');
?>

    <main class="main brand-page-main">
        <section id="first-screen" class="first-screen">
            <div class="top"
                 style="background-image: linear-gradient(0deg, rgba(21, 21, 21, 0.75), rgba(21, 21, 21, 0.75)), url(<?php echo carbon_get_the_post_meta('crb_image_bg'); ?>)">
                <div class="container">
                    <div class="left">
                        <h1><?php echo carbon_get_the_post_meta('crb_title'); ?></h1>
                        <span class="sub-title"><?php echo carbon_get_the_post_meta('crb_sub_title'); ?></span>
                        <p><?php echo carbon_get_the_post_meta('crb_sub_text'); ?></p>
                    </div>
                    <div class="right">
                        <?php echo wp_get_attachment_image(carbon_get_the_post_meta('crb_image'), 'full') ?>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <?php
                    $list = carbon_get_the_post_meta('crb_statistics');
                    $count = count($list)
                    ?>
                    <div class="statistic-list <?php echo 'columns-' . $count; ?>">

                        <?php foreach ($list as $item) { ?>
                            <div class="statistic-item">
                                <h3><?php echo $item['crb_link_name'] ?></h3>
                                <p><?php echo $item['crb_link'] ?></p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section><!-- #first-screen -->

        <section id="second-screen" class="second-screen">
            <div class="container">
                <div class="left">
                    <div class="content">
                        <?php if (have_posts()) : while (have_posts()) : the_post();
                            the_content();
                        endwhile;
                        endif; ?>
                    </div>
                    <div class="posts-list">
                        <?php $list = carbon_get_the_post_meta('crb_posts');

                        foreach ($list as $item) { ?>
                            <a class="post-item" href="<?php the_permalink($item['id']); ?>">
                                <img src="<?php echo get_the_post_thumbnail_url($item['id'], 'medium'); ?>"
                                     alt="<?php echo get_the_title($item['id']); ?>"/>
                                <div class="content">
                                    <div class="top">
                                        <span class="category">
                                            <?php foreach (get_the_category($item['id']) as $category) {
                                                echo $category->name . ' ';
                                            } ?>
                                        </span>
                                        <span class="breaker">/</span>
                                        <span class="date"><?php echo get_the_date('j F Y', $item['id']); ?></span>
                                    </div>
                                    <h5><?php echo get_the_title($item['id']); ?></h5>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="right">
                    <?php echo wpautop(carbon_get_the_post_meta('crb_brand_info')); ?>
                </div>
            </div>
        </section><!-- #second-screen -->

    </main><!-- .main -->

<?php
get_footer('transparent');
