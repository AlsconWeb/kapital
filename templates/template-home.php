<?php
/* Template Name: Home page*/

get_header('home');

?>

    <main class="home-page">
        <section id="section-left" class="section-left">
            <h3>Популярные новости</h3>


            <?php $args = array(
                'post_type' => 'post',
                'showposts' => '11',
                'offset' => '0',
                'date_query' => array(
//                    array(
//                        'year' => date('Y'),
//                        'week' => date('W'),
//                    ),
	                'after' => date('Y-m-d', strtotime('-3 days'))
                ),
                'category__not_in' => array(209, 219),
                'orderby' => 'post_views',
            );

            $the_query = new WP_Query($args);
            $i = 0;

            while ($the_query->have_posts()) : $the_query->the_post();
                if (($i % 3) == 0 && $i != 0) { ?>
                    <div class="adfox-sidebar">
                        <div id="adfox_1607290897662527<?php echo $i ?>"></div>
                        <script>
                           if (typeof window.Ya !== 'undefined') {
                              window.Ya.adfoxCode.createAdaptive({
                                 ownerId: 277709,
                                 containerId: 'adfox_1607290897662527<?php echo $i ?>',
                                 params: {
                                    pp: 'g',
                                    ps: 'engg',
                                    p2: 'geyb',
                                    puid1: ''
                                 }
                              }, ['desktop'], {
                                 tabletWidth: 768,
                                 phoneWidth: 576,
                                 isAutoReloads: false
                              });
                           } else {
                              console.error("sidebar: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ")
                           }
                        </script>
                    </div>
                <?php }
                ?>

                <div class="post">
                    <span class="date"><?php echo get_the_date('j F Y'); ?></span>
                    <a href="<?php the_permalink(); ?>"
                       class="title small-post-title"><?php echo get_the_title(); ?></a>
                </div>

                <?php
                $i++;
            endwhile; ?>


        </section><!-- #section-left -->

        <section id="section-right" class="section-right">
            <div class="main-banner">
                <div class="container">
                    <div class="img-wrapper">
                        <div class="banner-adfox">

                            <!--AdFox START-->
                            <!--WebSail-Advertisement-->
                            <!--Площадка: Kapital.uz / * / *-->
                            <!--Тип баннера: 1150x100-->
                            <!--Расположение: <верх страницы>-->
                            <div id="adfox_160729078850626540"></div>
                            <script>
                               if (typeof window.Ya !== 'undefined') {
                                  window.Ya.adfoxCode.createAdaptive({
                                     ownerId: 277709,
                                     containerId: 'adfox_160729078850626540',
                                     params: {
                                        pp: 'g',
                                        ps: 'engg',
                                        p2: 'geya',
                                        puid1: ''
                                     }
                                  }, ['desktop', 'tablet'], {
                                     tabletWidth: 768,
                                     phoneWidth: 576,
                                     isAutoReloads: false
                                  });
                               } else {
                                  console.error("header: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ")
                               }
                            </script>

                            <!--AdFox START-->
                            <!--WebSail-Advertisement-->
                            <!--Площадка: Kapital.uz / * / *-->
                            <!--Тип баннера: 640x300-->
                            <!--Расположение: <верх страницы>-->
                            <div id="adfox_160728941561475980"></div>
                            <script>
                               if (typeof window.Ya !== 'undefined') {
                                  window.Ya.adfoxCode.createAdaptive({
                                     ownerId: 277709,
                                     containerId: 'adfox_160728941561475980',
                                     params: {
                                        pp: 'g',
                                        ps: 'engg',
                                        p2: 'geyc',
                                        puid1: ''
                                     }
                                  }, ['phone'], {
                                     tabletWidth: 768,
                                     phoneWidth: 576,
                                     isAutoReloads: false
                                  });
                               } else {
                                  console.error("header: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ")
                               }
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <div id="main-section" class="main-section">
                <div class="container">
                    <h3 class="section-title">Лента новостей</h3>
                </div>
                <?php $args = array(
                    'post_type' => 'post',
                    'showposts' => '1',
                    'offset' => '0',
                    'category__not_in' => array(209, 219),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'main',
                            'field' => 'slug',
                            'terms' => 'main',
                        ),
                    ),
                );
                $the_query = new WP_Query($args); ?>

                <?php while ($the_query->have_posts()) :
                    $the_query->the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="main-post-wrapper">
                        <div class="img">
                            <?php echo the_post_thumbnail('full'); ?>
                        </div>
                        <div class="container">
                            <div class="desc-wrapper">
                                <h2 class="main-post-title"><?php echo get_the_title(); ?></h2>
                                <p class="main-post-desc"><?php echo get_the_excerpt() ?></p>
                            </div>
                        </div>
                    </a>
                <?php endwhile; ?>

                <div id="latest-posts-container" class="container small-container">
                    <?php $args = array(
                        'post_type' => 'post',
                        'showposts' => '6',
                        'offset' => '0',
                        'category__not_in' => array(209, 219),
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'main',
                                'operator' => 'NOT EXISTS'),
                        ),
                    );
                    $the_query = new WP_Query($args); ?>

                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                        <a href="<?php the_permalink(); ?>" class="small-post-wrapper">
                            <div class="img">
                                <?php echo the_post_thumbnail('full'); ?>
                            </div>
                            <div class="desc-wrapper">
                                <span class="date"><?php echo get_the_date('d M Y'); ?></span>
                                <h2 class="small-post-title"><?php echo get_the_title(); ?></h2>
                            </div>
                        </a>

                    <?php endwhile; ?>
                </div>

                <?php
                ## global $wp_query;
                if ($the_query->max_num_pages > 1) {
                    echo '<button id="more-latest-post-btn" class="button more-post-btn">Больше новостей</button>'; // you can use <a> as well
                } ?>
            </div><!-- #main-section -->

            <div class="main-banner">
                <div class="container">
                    <div class="img-wrapper">
                        <div class="banner-adfox">

                            <!--AdFox START-->
                            <!--WebSail-Advertisement-->
                            <!--Площадка: Kapital.uz / * / *-->
                            <!--Тип баннера: 1150x100-->
                            <!--Расположение: <верх страницы>-->
                            <div id="adfox_160729078850626541"></div>
                            <script>
                               if (typeof window.Ya !== 'undefined') {
                                  window.Ya.adfoxCode.createAdaptive({
                                     ownerId: 277709,
                                     containerId: 'adfox_160729078850626541',
                                     params: {
                                        pp: 'g',
                                        ps: 'engg',
                                        p2: 'geya',
                                        puid1: ''
                                     }
                                  }, ['desktop', 'tablet'], {
                                     tabletWidth: 768,
                                     phoneWidth: 576,
                                     isAutoReloads: false
                                  });
                               } else {
                                  console.error("header: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ")
                               }
                            </script>

                            <!--AdFox START-->
                            <!--WebSail-Advertisement-->
                            <!--Площадка: Kapital.uz / * / *-->
                            <!--Тип баннера: 640x300-->
                            <!--Расположение: <верх страницы>-->
                            <div id="adfox_160728941561475981"></div>
                            <script>
                               if (typeof window.Ya !== 'undefined') {
                                  window.Ya.adfoxCode.createAdaptive({
                                     ownerId: 277709,
                                     containerId: 'adfox_160728941561475981',
                                     params: {
                                        pp: 'g',
                                        ps: 'engg',
                                        p2: 'geyc',
                                        puid1: ''
                                     }
                                  }, ['phone'], {
                                     tabletWidth: 768,
                                     phoneWidth: 576,
                                     isAutoReloads: false
                                  });
                               } else {
                                  console.error("header: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ")
                               }
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <div id="video-section" class="video-section">
                <div class="container title-section">
                    <div class="section-title-wrapper">
                        <h3 class="section-title">Kapital TV</h3>
                    </div>
                </div>
                <?php $args = array(
                    'post_type' => 'post',
                    'showposts' => '4',
                    'offset' => '0',
                    'cat' => 219,
                );
                $the_query = new WP_Query($args);
                $i = 0;

                while ($the_query->have_posts()) : $the_query->the_post();

                    if ($i == 0) { ?>
                        <a href="<?php echo get_the_permalink(); ?>" class="main-post-wrapper">
                            <div class="container video-wrapper">
                                <div class="image-container">
                                    <div class="play-btn">
                                        <img class="blue-play-btn"
                                             src="<?php bloginfo('template_url'); ?>/assets/img/play-btn-blue.png"/>
                                    </div>
                                    <?php echo the_post_thumbnail('full'); ?>
                                    <div class="play-link">
                                        <span class="text">СМОТРЕТЬ</span>
                                        <img class="play-logo"
                                             src="<?php bloginfo('template_url'); ?>/assets/img/play-img-white.png"/>
                                    </div>
                                </div>
                            </div>
                            <div class="container desc-section-wrapper">
                                <div class="desc-wrapper">
                                    <h2 class="main-post-title"><?php echo get_the_title(); ?></h2>
                                    <p class="main-post-desc"><?php echo get_the_excerpt() ?></p>
                                </div>
                            </div>
                        </a>
                    <?php } else {
                        if ($i == 1) {
                            echo '<div class="container small-container">';
                        }
                        ?>
                        <a href="<?php the_permalink(); ?>" class="small-post-wrapper">
                            <div class="small-image-container">
                                <div class="play-btn">
                                    <img class="blue-play-btn"
                                         src="<?php bloginfo('template_url'); ?>/assets/img/play-btn-blue.png"/>
                                </div>
                                <?php echo the_post_thumbnail('full'); ?>
                                <div class="play-link">
                                    <span class="text">СМОТРЕТЬ</span>
                                    <img class="play-logo"
                                         src="<?php bloginfo('template_url'); ?>/assets/img/play-img-white.png"/>
                                </div>
                            </div>
                            <div class="desc-wrapper">
                                <span class="date"><?php echo get_the_date('d M Y'); ?></span>
                                <h2 class="small-post-title"><?php echo get_the_title(); ?></h2>
                                <p class="small-post-desc"><?php echo get_the_excerpt() ?></p>
                            </div>
                        </a>

                    <?php }

                    $i++;
                endwhile;
                if ($i > 0) {
                    echo '</div>';
                } ?>

                <div class="container">
                    <?php
                    ## global $wp_query;
                    if ($the_query->max_num_pages > 1) {
                        echo '<button id="video-people-post-btn" class="button">Больше новостей</button>'; // you can use <a> as well
                    } ?>
                </div>
            </div><!-- #video-section -->

            <div class="main-banner">
                <div class="container">
                    <div class="img-wrapper">
                        <div class="banner-adfox">

                            <!--AdFox START-->
                            <!--WebSail-Advertisement-->
                            <!--Площадка: Kapital.uz / * / *-->
                            <!--Тип баннера: 1150x100-->
                            <!--Расположение: <верх страницы>-->
                            <div id="adfox_160729078850626542"></div>
                            <script>
                               if (typeof window.Ya !== 'undefined') {
                                  window.Ya.adfoxCode.createAdaptive({
                                     ownerId: 277709,
                                     containerId: 'adfox_160729078850626542',
                                     params: {
                                        pp: 'g',
                                        ps: 'engg',
                                        p2: 'geya',
                                        puid1: ''
                                     }
                                  }, ['desktop', 'tablet'], {
                                     tabletWidth: 768,
                                     phoneWidth: 576,
                                     isAutoReloads: false
                                  });
                               } else {
                                  console.error("header: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ")
                               }
                            </script>

                            <!--AdFox START-->
                            <!--WebSail-Advertisement-->
                            <!--Площадка: Kapital.uz / * / *-->
                            <!--Тип баннера: 640x300-->
                            <!--Расположение: <верх страницы>-->
                            <div id="adfox_160728941561475982"></div>
                            <script>
                               if (typeof window.Ya !== 'undefined') {
                                  window.Ya.adfoxCode.createAdaptive({
                                     ownerId: 277709,
                                     containerId: 'adfox_160728941561475982',
                                     params: {
                                        pp: 'g',
                                        ps: 'engg',
                                        p2: 'geyc',
                                        puid1: ''
                                     }
                                  }, ['phone'], {
                                     tabletWidth: 768,
                                     phoneWidth: 576,
                                     isAutoReloads: false
                                  });
                               } else {
                                  console.error("header: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ")
                               }
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <div id="popular-section" class="popular-section">
                <div id="popular-posts-container" class="container">
                    <h3 class="section-title">Эксклюзивные новости</h3>
                    <?php $args = array(
                        'post_type' => 'post',
                        'showposts' => '3',
                        'offset' => '0',
                        'cat' => 221,
                    );
                    $the_query = new WP_Query($args); ?>

                    <?php while ($the_query->have_posts()) :
                        $the_query->the_post(); ?>
                        <a href="<?php the_permalink(); ?>" class="main-post-wrapper">
                            <div class="img">
                                <?php echo the_post_thumbnail('full'); ?>
                            </div>
                            <div class="desc-wrapper">
                                <h2 class="main-post-title"><?php echo get_the_title(); ?></h2>
                                <p class="main-post-desc"><?php echo get_the_excerpt() ?></p>
                            </div>
                        </a>

                    <?php endwhile; ?>
                </div>
                <?php
                ## global $wp_query;
                if ($the_query->max_num_pages > 1) {
                    echo '<button id="popular-people-post-btn" class="button">Больше новостей</button>'; // you can use <a> as well
                } ?>
            </div><!-- #popular-section -->

            <div id="people-section" class="people-section">
                <div id="people-posts-container" class="container">
                    <h3 class="section-title">Люди</h3>

                    <?php $args = array(
                        'post_type' => 'post',
                        'showposts' => '4',
                        'offset' => '0',
                        'cat' => 15,
                    );
                    $the_query = new WP_Query($args);
                    $i = 0;

                    while ($the_query->have_posts()) : $the_query->the_post();
                        if ($i == 0) { ?>
                            <a href="<?php the_permalink(); ?>" class="main-post-wrapper">
                                <div class="img">
                                    <div class="description-wrapper">
                                        <?php $title = carbon_get_the_post_meta('crb_show_content');
                                        if (!$title) { ?>
                                            <h3 class="title"><?php echo get_the_title(); ?></h3>
                                        <?php } ?>
                                    </div>
                                    <?php echo the_post_thumbnail('full'); ?>
                                </div>
                            </a>
                        <?php } else { ?>
                            <a href="<?php the_permalink(); ?>" class="second-post-wrapper">
                                <div class="img">
                                    <?php echo the_post_thumbnail('full'); ?>
                                </div>
                                <div class="desc-wrapper">
                                    <h2 class="main-post-title"><?php echo get_the_title(); ?></h2>
                                    <p class="main-post-desc"><?php echo get_the_excerpt() ?></p>
                                </div>
                            </a>
                        <?php }
                        $i++;
                    endwhile; ?>
                </div>
                <?php
                ## global $wp_query;
                if ($the_query->max_num_pages > 1) {
                    echo '<button id="more-people-post-btn" class="button">Больше новостей</button>'; // you can use <a> as well
                } ?>
            </div><!-- #people-section -->
        </section><!-- #section-right -->
    </main>

<?php

get_footer('home');
