<?php
/* Template Name: Adfox page*/

get_header( 'adfox' );
?>

	<main class="main adfox-page-main">
		<?php echo carbon_get_theme_option( 'crb_adfox' ); ?>
	</main><!-- .main -->

<?php
get_footer( 'adfox' );
