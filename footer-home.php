<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kapitaluz
 */

?>
<footer class="main-footer">
	<div class="container">
		<div class="telegram-section">
			<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/k-icon.png" alt="Site logo">
			<p class="text">Больше новостей<br> в Телеграме</p>
			<a href="https://t.me/KPTLUZ" target="_blank" class="btn">Подписаться</a>
		</div>
		<div class="main-footer__inner">
			<div class="top-wrapper">
				<div class="left-section">
					<a href="#" class="img-wrapper">
						<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/logo.svg" alt="Site logo">
					</a>
					<div class="social">
						<a class="social-link" href="https://t.me/KPTLUZ" target="_blank">
							<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/social/tg.svg" alt="telegram"/>
						</a>
						<a class="social-link" href="https://www.facebook.com/KPTLUZ" target="_blank">
							<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/social/fb.svg" alt="facebook"/>
						</a>
					</div>
				</div>
				<div class="right-section">
					<?php
					wp_nav_menu( [ 'theme_location' => 'menu-2' ] );
					?>
				</div>
			</div>

			<div class="bot-wrapper">
				<p class="copyright">© 2021 Kapital.uz | При использовании материалов гиперссылка на kapital.uz
					обязательна.</p>
				<p class="feetback">Мы открыты к обратной связи, предложениям и сотрудничеству с читателями и брендами.
					Связаться с нами можно написав на info@kapital.uz</p>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Y99T2EK1SC"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push( arguments );
	}

	gtag( 'js', new Date() );

	gtag( 'config', 'G-Y99T2EK1SC' );
</script>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</body>
</html>
