<?php
/**
 * kapitaluz functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package kapitaluz
 */
// AUTHOR


/**************/

// LOADMORE
function misha_my_load_more_scripts() {

	global $wp_query;

	// In most cases it is already included on the page and this line can be removed
	wp_enqueue_script( 'jquery' );

	// register our main script but do not enqueue it yet
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/js/myloadmore.js', [ 'jquery' ] );

	// now the most interesting part
	// we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'my_loadmore', 'misha_loadmore_params', [
		'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts'        => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
		'max_page'     => $wp_query->max_num_pages,
	] );

	wp_enqueue_script( 'my_loadmore' );

}

add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );

function misha_loadmore_ajax_handler() {

	// prepare our arguments for the query
	$args                = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged']       = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';

	// it is always better to use WP_Query but not here
	query_posts( $args );

	if ( have_posts() ) :

		// run the loop
		while ( have_posts() ): the_post();

			?>
			<div class="post loaded">
				<a class="img-wrapper" href="<?php the_permalink(); ?>"> <img
							src="<?php echo get_the_post_thumbnail_url( $post, 'full' ); ?>"
							alt="<?php the_title(); ?>"/>
				</a>
				<div class="post__content">
					<div class="post-top"><?php echo get_the_category_list(); ?><span class="breaker">/</span> <span
								class="post-date"><?php echo get_the_date( 'j F Y' ); ?></span></div>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</div>
			</div>
		<?php


		endwhile;

	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}


add_action( 'wp_ajax_loadmore', 'misha_loadmore_ajax_handler' ); // wp_ajax_{action}
add_action( 'wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler' ); // wp_ajax_nopriv_{action}


/*Load more*/
function load_more_scripts() {

	global $wp_query;

	wp_register_script( 'loadmore', get_template_directory_uri() . '/assets/js/loadmore.js', [ 'jquery' ] );

	wp_localize_script( 'loadmore', 'loadmore_params', [
		'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php',
		'posts'        => json_encode( $wp_query->query_vars ),
		'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
		'max_page'     => $wp_query->max_num_pages,
	] );

	wp_localize_script( 'loadmore', 'people_loadmore_params', [
		'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php',
		'posts'        => json_encode( $wp_query->query_vars ),
		'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
		'max_page'     => $wp_query->max_num_pages,
	] );

	wp_localize_script( 'loadmore', 'video_loadmore_params', [
		'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php',
		'posts'        => json_encode( $wp_query->query_vars ),
		'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
		'max_page'     => $wp_query->max_num_pages,
	] );

	wp_localize_script( 'loadmore', 'popular_loadmore_params', [
		'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php',
		'posts'        => json_encode( $wp_query->query_vars ),
		'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
		'max_page'     => $wp_query->max_num_pages,
	] );

	wp_enqueue_script( 'loadmore' );
}

add_action( 'wp_enqueue_scripts', 'load_more_scripts' );


//Latest posts
function latest_posts_loadmore_ajax_handler() {

	// prepare our arguments for the query
//    $args = json_decode(stripslashes($_POST['query']), true);
	$args['paged']          = $_POST['page'] + 1; // we need next page to be loaded
	$args['posts_per_page'] = 6;
	$args['post_type']      = 'post';
	$args['post_status']    = 'publish';


	// it is always better to use WP_Query but not here
	query_posts( $args );

	if ( have_posts() ) :

		echo '<div class="loadmore-add-container">';

		while ( have_posts() ): the_post(); ?>
			<a href="<?php the_permalink(); ?>" class="small-post-wrapper">
				<div class="img">
					<?php echo the_post_thumbnail( 'full' ); ?>
				</div>
				<div class="desc-wrapper">
					<span class="date"><?php echo get_the_date( 'd M Y' ); ?></span>
					<h2 class="small-post-title"><?php echo get_the_title(); ?></h2>
				</div>
			</a>
		<?php
		endwhile;
		echo '</div>';
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}

add_action( 'wp_ajax_latest_loadmore', 'latest_posts_loadmore_ajax_handler' ); // wp_ajax_{action}
add_action( 'wp_ajax_nopriv_latest_loadmore', 'latest_posts_loadmore_ajax_handler' ); // wp_ajax_nopriv_{action}

//People posts
function people_posts_loadmore_ajax_handler() {

	// prepare our arguments for the query
//    $args = json_decode(stripslashes($_POST['query']), true);
	$args['paged']          = $_POST['page'] + 1; // we need next page to be loaded
	$args['posts_per_page'] = 6;
	$args['post_type']      = 'post';
	$args['cat']            = 15;
	$args['post_status']    = 'publish';


	// it is always better to use WP_Query but not here
	query_posts( $args );

	if ( have_posts() ) :
		while ( have_posts() ): the_post(); ?>
			<a href="<?php the_permalink(); ?>" class="second-post-wrapper">
				<div class="img">
					<?php echo the_post_thumbnail( 'full' ); ?>
				</div>
				<div class="desc-wrapper">
					<h2 class="main-post-title"><?php echo get_the_title(); ?></h2>
					<p class="main-post-desc"><?php echo get_the_excerpt() ?></p>
				</div>
			</a>
		<?php
		endwhile;
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}

add_action( 'wp_ajax_people_loadmore', 'people_posts_loadmore_ajax_handler' ); // wp_ajax_{action}
add_action( 'wp_ajax_nopriv_people_loadmore', 'people_posts_loadmore_ajax_handler' ); // wp_ajax_nopriv_{action}


//Video posts
function video_posts_loadmore_ajax_handler() {

	// prepare our arguments for the query
//    $args = json_decode(stripslashes($_POST['query']), true);
	$args['paged']          = $_POST['page'] + 1; // we need next page to be loaded
	$args['posts_per_page'] = 3;
	$args['post_type']      = 'post';
	$args['cat']            = 219;
	$args['post_status']    = 'publish';


	// it is always better to use WP_Query but not here
	query_posts( $args );

	if ( have_posts() ) :
		while ( have_posts() ): the_post(); ?>
			<a href="<?php the_permalink(); ?>" class="small-post-wrapper">
				<div class="small-image-container">
					<div class="play-btn">
						<img class="blue-play-btn"
							 src="<?php bloginfo( 'template_url' ); ?>/assets/img/play-btn-blue.png"/>
					</div>
					<?php echo the_post_thumbnail( 'full' ); ?>
					<div class="play-link">
						<span class="text">СМОТРЕТЬ</span>
						<img class="play-logo"
							 src="<?php bloginfo( 'template_url' ); ?>/assets/img/play-img-white.png"/>
					</div>
				</div>
				<div class="desc-wrapper">
					<span class="date"><?php echo get_the_date( 'd M Y' ); ?></span>
					<h2 class="small-post-title"><?php echo get_the_title(); ?></h2>
					<p class="small-post-desc"><?php echo get_the_excerpt() ?></p>
				</div>
			</a>
		<?php
		endwhile;
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}

add_action( 'wp_ajax_video_loadmore', 'video_posts_loadmore_ajax_handler' ); // wp_ajax_{action}
add_action( 'wp_ajax_nopriv_video_loadmore', 'video_posts_loadmore_ajax_handler' ); // wp_ajax_nopriv_{action}

//Popular posts
function popular_posts_loadmore_ajax_handler() {
	$args = [
		'paged'          => $_POST['page'] + 1,
		'posts_per_page' => 3,
		'post_type'      => 'post',
		'post_status'    => 'publish',
		'cat'            => 221,
	];
	// it is always better to use WP_Query but not here
	query_posts( $args );


	if ( have_posts() ) :
		while ( have_posts() ): the_post(); ?>
			<a href="<?php the_permalink(); ?>" class="main-post-wrapper">
				<div class="img">
					<?php echo the_post_thumbnail( 'full' ); ?>
				</div>
				<div class="desc-wrapper">
					<h2 class="main-post-title"><?php echo get_the_title(); ?></h2>
					<p class="main-post-desc"><?php echo get_the_excerpt() ?></p>
				</div>
			</a>
		<?php
		endwhile;
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}

add_action( 'wp_ajax_popular_loadmore', 'popular_posts_loadmore_ajax_handler' ); // wp_ajax_{action}
add_action( 'wp_ajax_nopriv_popular_loadmore', 'popular_posts_loadmore_ajax_handler' ); // wp_ajax_nopriv_{action}


/*******************/


// Изменяем атрибут id у тега li
add_filter( 'nav_menu_item_id', 'filter_menu_item_css_id', 10, 4 );
function filter_menu_item_css_id( $menu_id, $item, $args, $depth ) {
	return $args->theme_location === 'menu-1' ? '' : $menu_id;
}

// Изменяем атрибут class у тега li
add_filter( 'nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4 );
function filter_nav_menu_css_classes( $classes, $item, $args, $depth ) {
	if ( $args->theme_location === 'menu-1' ) {
		$classes = [
			'main-menu__item',
		];
		if ( $item->current ) {
			$classes[] = 'main-menu__item-active';
		}
	}

	return $classes;
}

add_filter( 'nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4 );
function filter_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
	if ( $args->theme_location === 'menu-1' ) {
		$atts['class'] = 'menu-link';
		if ( $item->current ) {
			$atts['class'] .= ' menu-link--active';
		}
	}

	return $atts;
}


// Excerpt lenth
add_filter( 'excerpt_length', function () {
	return 30;
} );

add_filter( 'excerpt_more', function ( $more ) {
	return '...';
} );


## Отключает Гутенберг (новый редактор блоков в WordPress).
## ver: 1.2
if ( 'disable_gutenberg' ) {
	remove_theme_support( 'core-block-patterns' ); // WP 5.5

	add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );

	// отключим подключение базовых css стилей для блоков
	// ВАЖНО! когда выйдут виджеты на блоках или что-то еще, эту строку нужно будет комментировать
	remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );

	// Move the Privacy Policy help notice back under the title field.
	add_action( 'admin_init', function () {
		remove_action( 'admin_notices', [ 'WP_Privacy_Policy_Content', 'notice' ] );
		add_action( 'edit_form_after_title', [ 'WP_Privacy_Policy_Content', 'notice' ] );
	} );
}

/**
 * Отключаем принудительную проверку новых версий WP, плагинов и темы в админке,
 * чтобы она не тормозила, когда долго не заходил и зашел...
 * Все проверки будут происходить незаметно через крон или при заходе на страницу: "Консоль > Обновления".
 *
 * @see     https://wp-kama.ru/filecode/wp-includes/update.php
 * @author  Kama (https://wp-kama.ru)
 * @version 1.1
 */
if ( is_admin() ) {
	// отключим проверку обновлений при любом заходе в админку...
	remove_action( 'admin_init', '_maybe_update_core' );
	remove_action( 'admin_init', '_maybe_update_plugins' );
	remove_action( 'admin_init', '_maybe_update_themes' );

	// отключим проверку обновлений при заходе на специальную страницу в админке...
	remove_action( 'load-plugins.php', 'wp_update_plugins' );
	remove_action( 'load-themes.php', 'wp_update_themes' );

	// оставим принудительную проверку при заходе на страницу обновлений...
	//remove_action( 'load-update-core.php', 'wp_update_plugins' );
	//remove_action( 'load-update-core.php', 'wp_update_themes' );

	// внутренняя страница админки "Update/Install Plugin" или "Update/Install Theme" - оставим не мешает...
	//remove_action( 'load-update.php', 'wp_update_plugins' );
	//remove_action( 'load-update.php', 'wp_update_themes' );

	// событие крона не трогаем, через него будет проверяться наличие обновлений - тут все отлично!
	//remove_action( 'wp_version_check', 'wp_version_check' );
	//remove_action( 'wp_update_plugins', 'wp_update_plugins' );
	//remove_action( 'wp_update_themes', 'wp_update_themes' );

	/**
	 * отключим проверку необходимости обновить браузер в консоли - мы всегда юзаем топовые браузеры!
	 * эта проверка происходит раз в неделю...
	 * @see https://wp-kama.ru/function/wp_check_browser_version
	 */
	add_filter( 'pre_site_transient_browser_' . md5( $_SERVER['HTTP_USER_AGENT'] ), '__return_empty_array' );
}


if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'kapitaluz_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function kapitaluz_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on kapitaluz, use a find and replace
		 * to change 'kapitaluz' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'kapitaluz', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			[
				'menu-1' => esc_html__( 'Primary', 'kapitaluz' ),
				'menu-2' => esc_html__( 'Footer', 'kapitaluz' ),
			]
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			]
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'kapitaluz_custom_background_args',
				[
					'default-color' => 'ffffff',
					'default-image' => '',
				]
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Post formats
		add_theme_support( 'post-formats', [ 'aside', 'video' ] );


		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			[
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			]
		);
	}
endif;
add_action( 'after_setup_theme', 'kapitaluz_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function kapitaluz_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'kapitaluz_content_width', 640 );
}

add_action( 'after_setup_theme', 'kapitaluz_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kapitaluz_widgets_init() {
	register_sidebar(
		[
			'name'          => esc_html__( 'Sidebar', 'kapitaluz' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'kapitaluz' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		]
	);
}

add_action( 'widgets_init', 'kapitaluz_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function kapitaluz_scripts() {
	global $post;

	if ( ! is_page_template( 'templates/template-adfox.php' ) ) {
		wp_enqueue_style( 'kapitaluz-style', get_stylesheet_uri(), [], _S_VERSION );
		wp_enqueue_style( 'kapitaluz-fontas-css', get_template_directory_uri() . '/assets/css/fonts.css', [], _S_VERSION );
	}

	if ( is_page_template( 'templates/template-brand.php' ) ) {
		wp_enqueue_style( 'kapitaluz-brand-css', get_template_directory_uri() . '/assets/css/brand.css', [], _S_VERSION );
		wp_enqueue_script( 'kapitaluz-main', get_template_directory_uri() . '/assets/js/main.js', [], _S_VERSION, true );
	} elseif ( is_page_template( 'templates/template-home.php' ) ) {
		wp_enqueue_style( 'kapitaluz-home-css', get_template_directory_uri() . '/assets/css/home.css', [], _S_VERSION );
		wp_enqueue_script( 'kapitaluz-home', get_template_directory_uri() . '/assets/js/home.js', [], _S_VERSION, true );
	} else {
		wp_enqueue_style( 'kapitaluz-style2', get_template_directory_uri() . '/assets/css/main.css', [], _S_VERSION );
		wp_enqueue_script( 'kapitaluz-main', get_template_directory_uri() . '/assets/js/main.js', [], _S_VERSION, true );
	}

	if (
		is_category( [ 'biznes-akademiya', 'tbc-finedu' ] ) || in_category(
			[
				'biznes-akademiya',
				'tbc-finedu',
			],
			$post->ID
		)
	) {
		wp_dequeue_style( 'kapitaluz-style2' );

		wp_enqueue_script(
			'kapitaluz-main',
			get_template_directory_uri() . '/assets/js/main.js',
			[ 'jquery' ],
			_S_VERSION,
			true
		);

		wp_enqueue_script( 'kapitaluz-home', get_template_directory_uri() . '/assets/js/home.js', [ 'jquery' ], _S_VERSION, true );
		wp_enqueue_script( 'build', get_stylesheet_directory_uri() . '/assets/js/build.js', [ 'jquery' ], '1.0.0', true );

		wp_deregister_style( 'kapitaluz-style2' );

		wp_localize_script(
			'build',
			'iwp',
			[
				'ajaxUrl' => admin_url( 'admin-ajax.php' ),
			]
		);

		wp_enqueue_style( 'main-style-biznes', get_stylesheet_directory_uri() . '/assets/css/main_style_biznes.css', '', '1.0.0' );
	}

}

add_action( 'wp_enqueue_scripts', 'kapitaluz_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load Carbon fields.
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
	Container::make( 'post_meta', 'Page options' )
	         ->where( 'post_type', '=', 'page' )
	         ->where( 'post_template', '=', 'templates/template-brand.php' )
	         ->add_tab( __( 'First screen' ), [
		         Field::make( 'text', 'crb_title', __( 'Title' ) )
		              ->set_default_value( 'Зафар Хашимов' ),
		         Field::make( 'text', 'crb_sub_title', __( 'Title' ) )
		              ->set_default_value( 'Основатель сети супермаркетов korzinka.uz' ),
		         Field::make( 'textarea', 'crb_sub_text', __( 'Subtitle text' ) )
		              ->set_default_value( '«Когда вы лидер — вы ответственны не только за себя, вы отвечаете за всех, от отдельного человека до всей индустрии. И это, как не парадоксально, вполне справедливо»' ),
		         Field::make( 'image', 'crb_image', __( 'Image' ) ),
		         Field::make( 'image', 'crb_image_bg', __( 'Image BG' ) )
		              ->set_value_type( 'url' ),
		         Field::make( 'complex', 'crb_statistics', __( 'Statistics' ) )
		              ->add_fields( [
			              Field::make( 'text', 'crb_link_name', __( 'Title' ) )
			                   ->set_default_value( 'Более 20 лет' )
			                   ->set_width( 50 ),
			              Field::make( 'textarea', 'crb_link', __( 'Sub title' ) )
			                   ->set_default_value( 'Предпринимательского опыта в сфере продаж продуктов массового потребления и в ресторанном бизнесе' )
			                   ->set_width( 50 ),
		              ] ),
	         ] )
	         ->add_tab( __( 'Second screen' ), [
		         Field::make( 'rich_text', 'crb_brand_info', __( 'Brand info' ) ),
		         Field::make( 'association', 'crb_posts', __( 'Posts' ) )
		              ->set_types( [
			              [
				              'type'      => 'post',
				              'post_type' => 'post',
			              ],
		              ] ),
	         ] );

	Container::make( 'post_meta', 'Post settings' )
	         ->where( 'post_type', '=', 'post' )
	         ->add_fields( [
		         Field::make( 'checkbox', 'crb_show_content', __( 'Hidden Title' ) )
		              ->set_option_value( 'yes' ),
	         ] );
}

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_main_options' );
function crb_attach_theme_main_options() {
	Container::make( 'theme_options', __( 'Theme Options' ) )
	         ->add_fields( [
		         Field::make( 'textarea', 'crb_adfox', 'ADFOX Field' )
		              ->set_rows( 44 ),
	         ] );
}

add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
	require_once( 'vendor/autoload.php' );
	\Carbon_Fields\Carbon_Fields::boot();
}

add_action( 'pre_get_posts', 'my_view_filter' );
function my_view_filter( $query ) {
	if (
		! is_admin() &&
		$query->is_main_query() &&
		( $query->is_home() || $query->is_archive() || $query->is_search() )
	) {
		if ( isset( $_REQUEST['orderby'] ) ) {
			$order = $_REQUEST['orderby'];
		}
		if ( $order === 'views' ) {
			$query->set( 'meta_key', 'post_views_count' );
			$query->set( 'orderby', 'meta_value_num' );
			$query->set( 'order', 'DESC' );
		}
	}
}

/**
 * Add Support SVG File.
 *
 * @param array $mime_types Allowed Mime Types.
 *
 * @return array
 */
function mime_types( array $mime_types ): array {
	$mime_types['svg'] = 'image/svg+xml';

	return $mime_types;
}

add_filter( 'upload_mimes', 'mime_types' );

/**
 * Ajax Handler Load Post to biznes category.
 */
function lode_more_post_ajax_handler(): void {
	$term_id = ! empty( $_POST['term_id'] ) ?
		filter_var(
			wp_unslash( $_POST['term_id'] ),
			FILTER_SANITIZE_NUMBER_INT
		) : null;
	$page    = ! empty( $_POST['page'] ) ?
		filter_var(
			wp_unslash( $_POST['page'] ),
			FILTER_SANITIZE_NUMBER_INT
		) : null;
	$nonce   = ! empty( $_POST['nonce'] ) ?
		filter_var(
			wp_unslash( $_POST['nonce'] ),
			FILTER_SANITIZE_STRING
		) : null;


	if ( ! wp_verify_nonce( $nonce, 'iwp_load_more_' . $term_id ) ) {
		wp_send_json_error( [ 'message' => __( 'Не верный nonce код', 'kapitaluz' ) ] );
	}

	$arg       = [
		'post_type'     => 'post',
		'post_per_page' => 4,
		'page'          => ( $page + 1 ),
		'tax_query'     => [
			[
				'taxonomy' => 'category',
				'field'    => 'id',
				'terms'    => $term_id,
			],
		],
	];
	$postsData = '';
	$query     = new WP_Query( $arg );
	ob_start();
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$id = get_the_ID();
			?>
			<div class="col">
				<div class="news-item">
					<?php
					if ( has_post_thumbnail( $id ) ) :
						the_post_thumbnail();
						?>
					<?php else : ?>
						<img src="//via.placeholder.com/561x348" alt="No Image">
					<?php endif; ?>
					<div class="description">
						<p class="data"><?php the_time( 'j M Y' ); ?></p>
						<h3 class="title"><?php the_title(); ?></h3>
						<p class="desc"><?php echo esc_html( get_the_excerpt( $id ) ); ?></p>
						<?php
						$terms = wp_get_post_terms( $id, 'category' );
						foreach ( $terms as $termPost ) :
							?>
							<a
									class="meta"
									href="<?php echo esc_url( get_term_link( $termPost->term_id ) ); ?>"
							>#<?php echo esc_html( $termPost->name ); ?></a>
						<?php endforeach; ?>
					</div>
					<a class="link" href="<?php the_permalink(); ?>"></a>
				</div>
			</div>
			<?php
		}
		wp_reset_postdata();
	} else {
		wp_send_json_error( [ 'message' => __( 'Постов не найдено', 'kapitaluz' ) ] );
	}
	$postsData = ob_get_clean();

	wp_send_json_success(
		[
			'post_data' => $postsData,
			'page_num'  => ( $page + 1 ),
		]
	);

}

add_action( 'wp_ajax_show_more_posts', 'lode_more_post_ajax_handler' );
add_action( 'wp_ajax_nopriv_show_more_posts', 'lode_more_post_ajax_handler' );


function add_settings_page() {
	if ( function_exists( 'acf_add_options_page' ) ) {
		acf_add_options_page(
			[
				'page_title' => __( 'Настройки кнопок в хедере', 'kapitaluz' ),
				'menu_title' => __( 'Настройки хедера', 'kapitaluz' ),
				'menu_slug'  => 'theme-header-settings',
				'capability' => 'edit_posts',
				'redirect'   => false,
			]
		);
	}
}

add_action( 'admin_menu', 'add_settings_page', 10 );
