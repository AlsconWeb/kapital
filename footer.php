<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kapitaluz
 */

?>
        <footer class="main-footer">
            <div class="container">
                <div class="main-footer__inner">
                    <div class="footer-top">
                        <a href="/" class="img-wrapper logo"> <img src="<?php bloginfo('template_url'); ?>/assets/img/logo.svg" alt="logo" /> </a>
                        <ul class="footer-menu">
                            <li class="footer-menu__item"><a href="/o-proekte">О проекте</a></li>
                            <li class="footer-menu__item"><a href="https://drive.google.com/file/d/1SV9vBZH01xBsAZhjLrpheeegqmND77A8/view">Реклама</a></li>
                            <li class="footer-menu__item"><a href="/kontakty">Контакты</a></li>
                        </ul>
                    </div>
                    <div class="footer-bottom"><p>&copy; <?php echo date("Y"); ?> Kapital.uz | При использовании материалов гиперссылка на Kapital.uz обязательна.</p></div>
                </div>
            </div>
        </footer>

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Y99T2EK1SC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-Y99T2EK1SC');
</script>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</body>
</html>
