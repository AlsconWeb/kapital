<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kapitaluz
 */


get_header();
?>
<main>
	<section class="article">
		<div class="container">
			<div class="main-post__inner">
				<div class="main-post__right">
					<div class="main-post-content">
						<div class="main-post-right">
							<h1 class="article-title"><?php wp_title(''); ?></h1>
							<hr>
							<div class="article-content"><?php the_content(); ?></div>
						</div>
					</div>
				</div>
				<div class="main-post-ads">
				<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
get_footer();
