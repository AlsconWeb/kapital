<?php
/**
 * Created 28.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 * @package kapitaluz
 */

$biznesCat = [ 'biznes-akademiya' ];
$termObj   = [];

if ( is_singular( 'post' ) ) {
	$postTerms = wp_get_post_terms( get_the_ID(), 'category' );
	foreach ( $postTerms as $post_term ) {
		if ( in_array( $post_term->slug, $biznesCat, true ) ) {
			$termObj = $post_term;
		}
	}
} else {
	$termObj = get_queried_object() ?? false;
}


$contentBanner = get_field( 'kontent_bannera', $termObj );
$bannerBg      = get_field( 'color_bg', $termObj );
$imageBg       = get_field( 'image', $termObj );
$image         = get_field( 'image_right', $termObj );
$advantage     = get_field( 'advantage', $termObj );
$description   = get_field( 'description', $termObj );

$style = '';
if ( $imageBg ) {
	$style = 'background-image: url("' . $imageBg['url'] . '");';
}

?>
<div class="banner" style="<?php echo esc_attr( $style ); ?> background-color: #bfb7b0;">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6">
				<div class="description">
					<?php echo wp_kses_post( $contentBanner ); ?>
				</div>
			</div>
			<?php if ( ! empty( $image ) ) : ?>
				<div class="col-12 col-lg-6">
					<div class="img">
						<img
								src="<?php echo esc_url( $image['url'] ); ?>"
								alt="<?php echo esc_attr( $image['title'] ); ?>">
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php if ( ! empty( $advantage ) ) : ?>
	<div class="advantage">
		<div class="container">
			<div class="row">
				<?php foreach ( $advantage as $item ) : ?>
					<div class="col">
						<?php echo wp_kses_post( $item['text'] ); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if ( ! empty( $description ) ) : $class = ''; ?>
	<div class="description-block">
		<div class="container">
			<div class="row">
				<?php foreach ( $description as $key => $item ) : ?>
					<?php
					if ( 0 === $key ) {
						$class = 'col-xxl-8 col-xl-8 col-lg-7 col-12';
					} else {
						$class = 'col-xxl-4 col-xl-4 col-lg-5 col-12';
					}
					?>
					<div class="<?php echo esc_attr( $class ); ?>">
						<?php echo wp_kses_post( $item['text'] ); ?>
					</div>
				<?php endforeach; ?>
				<div class="col-12">
					<a class="button" href="#">
						<?php esc_html_e( 'Скрыть описание спецпроекта', 'kapitaluz' ); ?>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
