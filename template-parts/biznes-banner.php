<?php
/**
 * Created 13.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package kapitaluz
 */

$binesCat = [ 'biznes-akademiya', 'tbc-finedu' ];

if ( is_singular( 'post' ) ) {
	$postTerms = wp_get_post_terms( get_the_ID(), 'category' );
	foreach ( $postTerms as $post_term ) {
		if ( in_array( $post_term->slug, $binesCat, true ) ) {
			$termObj = $post_term;
		}
	}
} else {
	$termObj = get_queried_object() ?? false;
}


$contentBanner = get_field( 'kontent_bannera', $termObj );
$bannerBg      = get_field( 'color_bg', $termObj );
$imageBg       = get_field( 'image', $termObj );

$style = '';
if ( $imageBg ) {
	$style = 'background-image: url("' . $imageBg['url'] . '");';
}
?>
<div class="banner" style="<?php echo esc_attr( $style ); ?> background-color: <?php echo esc_attr( $bannerBg ); ?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="description">
					<?php echo wp_kses_post( $contentBanner ); ?>
				</div>
			</div>
		</div>
	</div>
</div>