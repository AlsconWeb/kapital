<?php
/**
 * The template for displaying all single posts
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package kapitaluz
 */


if ( in_category( [ 'biznes-akademiya', 'tbc-finedu' ], get_the_ID() ) ) {
	get_header( 'biznes' );

	if ( in_category( [ 'biznes-akademiya' ], get_the_ID() ) ) {
		get_template_part( 'template-parts/biznes', 'academya' );
	} else {
		get_template_part( 'template-parts/biznes', 'banner' );
	}

} else {
	get_header();
}


$product_terms = get_the_terms( $post->ID, 'person' );
?>
	<main>
		<section class="article">
			<div class="container">
				<div class="main-post__inner">
					<div class="main-post__right">
						<div class="main-post-content">
							<div class="main-post-right">
								<span class="post-date"><?php echo get_the_date( 'j.n.Y | H:i' ); ?></span>
								<h1 class="article-title"><?php the_title(); ?></h1>
								<?php
								if ( ! empty( $product_terms ) ) {
									foreach ( $product_terms as $prod_term ) {

										echo sprintf(
											'<div class="author-box">
														<div class="img-wrapper">
															<img src="%s" alt="image" />
														</div>
														<div class="author-content">
															<h5><a href="%s">%s</a></h5>
															<p>%s</p>
														</div>
													</div>',
											esc_url( z_taxonomy_image_url( $prod_term->term_id ) ),
											esc_url( get_term_link( $prod_term ) ),
											esc_attr( $prod_term->name ),
											esc_html( $prod_term->description )
										);
									}
								}
								?>
								<p class="excerpt"><?php echo esc_html( get_the_excerpt() ); ?></p>
								<div class="img-wrapper post-img">
									<img
											src="<?php echo esc_url( get_the_post_thumbnail_url( $post, 'full' ) ); ?>"
											alt="<?php the_title(); ?>"/>
								</div>
								<p class="img-copyright"><?php the_post_thumbnail_caption( $post ); ?></p>
								<div class="article-content"><?php the_content(); ?></div>
								<div class="share-box">
									<p>поделиться:</p>
									<div class="links-wrapper">
										<a
												href="https://www.facebook.com/sharer.php?u=<?php echo esc_url( "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ); ?>"
												target="_blank" class="img-wrapper fb">
											<img
													width="7" height="14"
													src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/icons/fb-w.svg' ); ?>"
													alt="fb"/>
										</a>
										<a
												href="http://vk.com/share.php?u=<?php echo esc_url( "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ); ?>"
												target="_blank" class="img-wrapper wk">
											<img
													width="17" height="10"
													src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/icons/wk-w.svg' ); ?>"
													alt="wk"/>
										</a>
										<a
												href="https://t.me/share/url?url=<?php echo esc_url( "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ); ?>"
												target="_blank" class="img-wrapper tg">
											<img
													width="16" height="13"
													src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/icons/tg-w.svg' ); ?>"
													alt="tg"/>
										</a>
									</div>

								</div>
								<div
										class="tg-box" onclick="window.open('https://t.me/KPTLUZ','new_window');"
										style="cursor: pointer;">
									<div class="tg-box__content">
										<p>
											Больше новостей про финансы и бизнес в Телеграм-канале <span>@</span>KPTLUZ
										</p>
									</div>
									<div class="img-wrapper">
										<img
												src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/icons/tg-i.svg' ); ?>"
												alt="tg"/>
									</div>
								</div>
								<div class="article-ads"></div>

								<div class="adfox-video-mob">
									<!--AdFox START-->
									<!--WebSail-Advertisement-->
									<!--Площадка: Kapital.uz / Kapital.uz / inpage only video-->
									<!--Категория: <не задана>-->
									<!--Баннер: <не задана>-->
									<!--Тип баннера: Inpage Only Video-->

									<div id="adfox_video"></div>
									<script>
										window.Ya.adfoxCode.create( {
											ownerId: 277709,
											containerId: 'adfox_video',
											params: {
												p1: 'cnhal',
												p2: 'gsfd',
												pfc: 'dohgb',
												pfb: 'jkzay',
												puid1: '',
												insertAfter: undefined,
												insertPosition: '0',
												stick: false,
												stickTo: 'auto',
												stickyColorScheme: 'light'
											}
										} );
									</script>
								</div>


								<div class="latest-news article-related-posts">
									<div class="container">
										<h3 class="section-title">Материалы по теме</h3>
										<div class="lates-news__inner">
											<div class="big-post-wrapper">
												<?php
												$related = get_posts(
													[
														'category__in' => wp_get_post_categories( $post->ID ),
														'numberposts'  => 3,
														'post__not_in' => [ $post->ID ],
													]
												);
												if ( $related ) {
													foreach ( $related as $post ) {
														setup_postdata( $post );
														?>

														<div class="post">
															<a class="img-wrapper" href="<?php the_permalink(); ?>">
																<img
																		src="<?php echo esc_url( get_the_post_thumbnail_url( $post, 'full' ) ); ?>"
																		alt="<?php the_title(); ?>"/>
															</a>
															<div class="post__content">
																<div class="post-top">
																	<?php echo get_the_category_list(); ?>
																	<span class="breaker">/</span>
																	<span class="post-date"><?php echo get_the_date( 'j F Y' ); ?></span>
																</div>
																<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
															</div>
														</div>
														<?php
													}
												}
												wp_reset_postdata();
												?>
											</div>
											<a href="#" class="load-more-link"> Показать еще </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="main-post-ads">
						<?php get_sidebar(); ?>
						<div class="popular-posts">
							<h4 class="sidesection-title">Популярное</h4>
							<div class="popular-posts-wrapper">
								<?php
								$args = [
									'post_type' => 'post',
									'showposts' => '4',
									'offset'    => '1',
									'tax_query' => [
										[
											'category' => '',
										],
									],
								];

								$the_query = new WP_Query( $args );
								?>

								<?php
								while ( $the_query->have_posts() ) :
									$the_query->the_post();
									?>


									<div class="post">
										<div class="post__content">
											<div class="post-top"><?php echo get_the_category_list(); ?><span
														class="breaker">/</span> <span
														class="post-date"><?php echo get_the_date( 'j F Y' ); ?></span>
											</div>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
						</div>
						<div class="latest-posts">
							<h4 class="sidesection-title">Последние новости</h4>
							<div class="popular-posts-wrapper">
								<?php
								$args = [
									'post_type' => 'post',
									'showposts' => '4',
									'offset'    => '1',
									'tax_query' => [
										[
											'category' => '',
										],
									],
								];

								$the_query = new WP_Query( $args );
								?>

								<?php
								while ( $the_query->have_posts() ) :
									$the_query->the_post();
									?>
									<div class="post">
										<div class="post__content">
											<div class="post-top"><?php echo get_the_category_list(); ?><span
														class="breaker">/</span> <span
														class="post-date"><?php echo get_the_date( 'j F Y' ); ?></span>
											</div>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

<?php
get_footer();
