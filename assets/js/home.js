jQuery(document).ready(function($){
	if($('#burger-btn').length){
		const burgerMenuButton = document.getElementById('burger-btn');
		const navMenu = document.getElementById('nav-menu');
		const srcButton = document.getElementById('src-button');
		const searchMenu = document.getElementById('src-menu');
		const bodyTag = $('body');
		console.log(bodyTag)
		burgerMenuButton.addEventListener('click', function (event) {
			bodyTag.toggleClass('menu-open');
			this.classList.toggle('btn-active');
			navMenu.classList.toggle('menu-active');
			searchMenu.classList.remove('search-menu-active');
			const link = document.querySelectorAll('.link');
			link.forEach(item => item.onclick = () => navMenu.classList.toggle('menu-active'));
		})
		srcButton.addEventListener('click', function (event) {
			this.classList.toggle('search-btn-active')
			searchMenu.classList.toggle('search-menu-active')
			navMenu.classList.remove('menu-active')
		})
	}
	if($('.banner').length){
		var bgBanner = $('.banner').css('background-color');
		$('header').css('background-color', '');
		$('#burger-btn').click(function(){
			if($(this).hasClass('btn-active')){
				$('header').css('background-color', bgBanner);
				$('.burger-menu').css('background-color', bgBanner);
			}else{
				$('header').css('background-color', 'transparent');
				$('.burger-menu').css('background-color', 'transparent');
			}
		}); 
		$('#src-button').click(function(){
			if($(this).hasClass('search-btn-active')){
				$('header').css('background-color', bgBanner);
				$('.burger-menu').css('background-color', bgBanner);
			}else{
				$('header').css('background-color', 'transparent');
				$('.burger-menu').css('background-color', 'transparent');
			}
		});
		$(document).on("scroll",function(){
			if($(document).scrollTop()>70){
				$('header').css('background-color', bgBanner);
				$('#burger-btn').click(function(){
					if($(this).hasClass('btn-active')){
						$('.burger-menu').css('background-color', bgBanner);
					}else{
						$('header').css('background-color', bgBanner);
						$('.burger-menu').css('background-color', 'transparent');
					}
				}); 
				$('#src-button').click(function(){
					if($(this).hasClass('search-btn-active')){
						$('.burger-menu').css('background-color', bgBanner);
					}else{
						$('header').css('background-color', bgBanner);
						$('.burger-menu').css('background-color', 'transparent');
					}
				});
			}
			else{
				$('header').css('background-color', '');
				$('#burger-btn').click(function(){
					if($(this).hasClass('btn-active')){
						$('header').css('background-color', bgBanner);
						$('.burger-menu').css('background-color', bgBanner);
					}else{
						$('header').css('background-color', 'transparent');
						$('.burger-menu').css('background-color', 'transparent');
					}
				});
				$('#src-button').click(function(){
					if($(this).hasClass('search-btn-active')){
						$('header').css('background-color', bgBanner);
						$('.burger-menu').css('background-color', bgBanner);
					}else{
						$('header').css('background-color', 'transparent');
						$('.burger-menu').css('background-color', 'transparent');
					}
				});
			}
		});
	}
	if($('.description-block').length){
		$('.description-block .button').click(function(e){
			e.preventDefault();
			if($(this).parents('.row').find('.col-12:nth-of-type(-n+2)').hasClass('hide')){
				$(this).parents('.row').find('.col-12:nth-of-type(-n+2)').removeClass('hide')
				$(this).text('Скрыть описание спецпроекта');
			}else{
				$(this).parents('.row').find('.col-12:nth-of-type(-n+2)').addClass('hide')
				$(this).text('Покозать описание спецпроекта');
				
			}
		})
		
	}
});









