jQuery(document).ready(function($){
	if($('.js-searchbar-toggle').length){
		const elSearchButtonToggler = document.querySelector( '.js-searchbar-toggle' );
		const elSearchBar = document.querySelector( '.js-search-form-wrapper' );
		const elBurgerButtonToggler = document.querySelector( '.js-burger-toggler' );
		const elMobNav = document.querySelector( '.js-mob-nav' );
		elBurgerButtonToggler.addEventListener( 'click', function() {
			this.classList.toggle( 'active' );
			elMobNav.classList.toggle( 'active' );
			elSearchButtonToggler.click();
		});
		elSearchButtonToggler.addEventListener( 'click', function() {
			this.classList.toggle( 'active' );
			elSearchBar.classList.toggle( 'active' );
		} );
	}
});









jQuery( document ).ready( function( $ ) {
	/**
	 * Disable Load More button
	 */

	function loadMoreBtn() {
		let countAllPost = Number( $( '.button.read-more' ).data( 'all_post' ) );
		let countOutputPost = Number( $( '.news .col' ).length );

		if ( countOutputPost >= countAllPost ) {
			$( '.button.read-more' ).hide();
		}
	}

	loadMoreBtn();

	/**
	 * Ajax Lode More post.
	 */
	$( '.button.read-more' ).click( function( e ) {
		e.preventDefault();

		let data = {
			action: 'show_more_posts',
			term_id: $( this ).data( 'term_id' ),
			page: $( this ).data( 'page_num' ),
			nonce: $( this ).data( 'nonce' ),
		};

		$.ajax( {
			type: 'POST',
			url: iwp.ajaxUrl,
			data: data,
			success: function( res ) {
				if ( res.success ) {
					$( '#more-content' ).append( res.data.post_data );
					$( '.button.read-more' ).data( 'page_num', res.data.page_num );
					loadMoreBtn();
				} else {
					alert( res.data.message );
				}

			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			},
		} );
	} );

} );