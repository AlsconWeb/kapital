jQuery(function($){
   /*Latest*/
   $('#more-latest-post-btn').click(function(){

      console.log('Click')
      let button = $(this),
         data = {
            'action': 'latest_loadmore',
            'query': loadmore_params.posts, // that's how we get params from wp_localize_script() function
            'page' : loadmore_params.current_page
         };

      $.ajax({ // you can also use $.post here
         url : loadmore_params.ajaxurl, // AJAX handler
         data : data,
         type : 'POST',
         beforeSend : function ( xhr ) {
            button.text('Загрузка...'); // change the button text, you can also add a preloader image
         },
         success : function( data ){
            if( data ) {
               console.log(data)
               button.text('Показать еще');
               $('#latest-posts-container').append(data); // insert new posts
               loadmore_params.current_page++;

               if ( loadmore_params.current_page == loadmore_params.max_page )
                  button.remove(); // if last page, remove the button

               // you can also fire the "post-load" event here if you use a plugin that requires it
               // $( document.body ).trigger( 'post-load' );
            } else {
               button.remove(); // if no data, remove the button as well
            }
         }
      });
   });

   /*People*/
   $('#more-people-post-btn').click(function(){

      console.log('Click')
      let button = $(this),
         data = {
            'action': 'people_loadmore',
            'query': people_loadmore_params.posts, // that's how we get params from wp_localize_script() function
            'page' : people_loadmore_params.current_page
         };

      $.ajax({ // you can also use $.post here
         url : people_loadmore_params.ajaxurl, // AJAX handler
         data : data,
         type : 'POST',
         beforeSend : function ( xhr ) {
            button.text('Загрузка...'); // change the button text, you can also add a preloader image
         },
         success : function( data ){
            if( data ) {
               console.log(data)
               button.text('Показать еще');
               $('#people-posts-container').append(data); // insert new posts
               people_loadmore_params.current_page++;

               if ( people_loadmore_params.current_page == people_loadmore_params.max_page )
                  button.remove(); // if last page, remove the button

               // you can also fire the "post-load" event here if you use a plugin that requires it
               // $( document.body ).trigger( 'post-load' );
            } else {
               button.remove(); // if no data, remove the button as well
            }
         }
      });
   });

   /*Video*/
   $('#video-people-post-btn').click(function(){

      console.log('Click')
      let button = $(this),
         data = {
            'action': 'people_loadmore',
            'query': video_loadmore_params.posts, // that's how we get params from wp_localize_script() function
            'page' : video_loadmore_params.current_page
         };

      $.ajax({ // you can also use $.post here
         url : people_loadmore_params.ajaxurl, // AJAX handler
         data : data,
         type : 'POST',
         beforeSend : function ( xhr ) {
            button.text('Загрузка...'); // change the button text, you can also add a preloader image
         },
         success : function( data ){
            if( data ) {
               console.log(data)
               button.text('Показать еще');
               $('#video-section').append(data); // insert new posts
               video_loadmore_params.current_page++;

               if ( video_loadmore_params.current_page == video_loadmore_params.max_page )
                  button.remove(); // if last page, remove the button

               // you can also fire the "post-load" event here if you use a plugin that requires it
               // $( document.body ).trigger( 'post-load' );
            } else {
               button.remove(); // if no data, remove the button as well
            }
         }
      });
   });

   /*Popular*/
   $('#popular-people-post-btn').click(function(){

      console.log('Click')
      let button = $(this),
         data = {
            'action': 'popular_loadmore',
            'query': popular_loadmore_params.posts, // that's how we get params from wp_localize_script() function
            'page' : popular_loadmore_params.current_page
         };

      $.ajax({ // you can also use $.post here
         url : popular_loadmore_params.ajaxurl, // AJAX handler
         data : data,
         type : 'POST',
         beforeSend : function ( xhr ) {
            button.text('Загрузка...'); // change the button text, you can also add a preloader image
         },
         success : function( data ){
            if( data ) {
               console.log(data)
               button.text('Показать еще');
               $('#popular-posts-container').append(data); // insert new posts
               popular_loadmore_params.current_page++;

               if ( popular_loadmore_params.current_page == popular_loadmore_params.max_page )
                  button.remove(); // if last page, remove the button

               // you can also fire the "post-load" event here if you use a plugin that requires it
               // $( document.body ).trigger( 'post-load' );
            } else {
               button.remove(); // if no data, remove the button as well
            }
         }
      });
   });
});