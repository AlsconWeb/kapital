<?php
/*
Template name: Full Page (No Sidebar)
*/

get_header();
?>

<?php 
$arr = [
	[
		"id" => 1,
		"name" => "Хикмат Абдурахманов",
		"job" => "Со-основатель холдинга HM Partners",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/kap.jpg",
		"quote" => "Дерзать, не стесняться, ставить самую высшую планку и двигаться к ней."
	],
	[
		"id" => 2,
		"name" => "Бобир Акилханов",
		"job" => "Основатель Missed.com и Jafton.com",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/bob.jpg",
		"quote" => "Надо быть более терпеливым."
	],
	[
		"id" => 3,
		"name" => "Роман Сайфулин",
		"job" => "Руководитель сети супермаркетов Makro",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/roman.jpg",
		"quote" => "Ничего не бояться. Только страх и неуверенность в себе нас останавливает быть лучше."
	],
	[
		"id" => 4,
		"name" => "Акмаль Пайзиев",
		"job" => "Основатель MyTaxi, Express24 и Workly",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/akmal.jpg",
		"quote" => "Я бы посоветовал себе 20-летнему сдавать документы в университет Stanford и уехать учиться туда, понимая также что это была бы совсем иная линия развития жизни."
	],
	[
		"id" => 5,
		"name" => "Тимур Мусин",
		"job" => "Основатель сети ресторанов Caravan Group и development.uz",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/timur.jpg",
		"quote" => "Я поздно понял, что бизнесом движут потоки клиентов. Больше времени всегда уделялось эстетичности, к примеру креативу подачи блюд, нежели количеству. Мне всегда советовали открыть три столовые на 2000 человек, которые однозначно принесли бы больше прибыли, чем 10 ресторанов."
	],
	[
		"id" => 6,
		"name" => "Лазиз Адхамов",
		"job" => "Руководитель SAP Uzbekistan, со-основатель Book Café",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/laziz.jpg",
		"quote" => "Я не жалею, как проживаю свою жизнь, при этом я бы посоветовал экономить время. Так как я очень много просидел за компьютерными играми в этом возрасте."
	],
	[
		"id" => 7,
		"name" => "Фарход Махмудов",
		"job" => "Основатель платежной системы OSON",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/farhod.jpg",
		"quote" => "Не торопись, больше изучай те предметы,  которые понимаешь и очень интересны лично для тебя, поставь себе большую цель, ту, которая кажется недостижимой. Не бойся этой цели, даже если уверен что никогда такого не добьёшься. Через время ты поймёшь, что нет нереальных целей, есть неправильное планирование и твой собственный страх. Преодолей это и все получится!"
	],
	[
		"id" => 8,
		"name" => "Мурад Назаров",
		"job" => "Основатель строительно-девелоперской компании Murad Buildings",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/murad.jpg",
		"quote" => "Жизнь не сложная, а очень простая и справедливая. Не бойся мечтать, строить планы. Верь, что все реально. Встречай все с улыбкой и стремись к своим целям."
	],
	[
		"id" => 9,
		"name" => "Санжар Максудов",
		"job" => "Основатель Smart Group",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/sanjar.jpg",
		"quote" => "Не теряй время, выбери то, что тебе действительно интересно и занимайся только этим. Страхов не существует – все только в нашей голове. Займись саморазвитием, психологией. Если заниматься тем, что тебе нравится, при этом быть в потоке и прокачивать свои навыки – никаких пределов нет."
	],
	[
		"id" => 10,
		"name" => "Ойбек Шодиев",
		"job" => "Генеральный директор девелоперской компании First Development Group",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/oybek.jpg",
		"quote" => "Я бы посоветовал развивать личностные качества, инвестировать в свои знания, заниматься спортом, не боятся и ставить большие цели перед собой и поэтапно достигать их."
	],
	[
		"id" => 11,
		"name" => "Улугбек Холматов",
		"job" => "Основатель цифровой стоматологии Stomaservice",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/ulugbek.jpg",
		"quote" => "Пусть прочитает мое интервью – на многое обратит свое внимание вовремя. С теми знаниями и опытом, которыми я сегодня обладаю – он мог бы справиться со всем не за 30 лет, как я, а за 5 лет."
	],
	[
		"id" => 12,
		"name" => "Лазиз Гулямов",
		"job" => "Сооснователь Pie Republic и гастро-бара «Пастернак»",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/glaziz.jpg",
		"quote" => "Делай всё по своему. Не слушай никого, учись всему что подвернётся, не бойся ошибаться и допускать ошибок."
	],
	[
		"id" => 13,
		"name" => "Ахмад Мелибаев",
		"job" => "Основатель Les Ailes и Chopar Pizza",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/ahmad.jpg",
		"quote" => "Не спешить жить – наслаждаться каждым моментом. Не бояться будущего. Рисковать и идти вперед."
	],
	[
		"id" => 14,
		"name" => "Вячеслав Кан",
		"job" => "Основатель Torg.uz",
		"imgsrc" => "https://kapital.uz/wp-content/uploads/2021/01/kan.jpg",
		"quote" => "Не бояться проявлять инициативу."
	]
];
?>

<section class="rubric-section">
	<div class="container">
		<h1 class="section-title">14 бизнесменов поделились собственными советами себе в двадцать лет</h1>
		<div class="page-desc">
			<p>Большинство начинающих людей в бизнесе так или иначе сталкиваются с трудностями, нехваткой опыта, идей, а где-то и мотивации. Kapital.uz поговорил с крупными бизнесменами Узбекистана, достигших определенных высот и достижений, и задал им единственный вопрос: "Какой совет вы дали бы себе в 20 лет?". Вот что из этого получилось:
			</p>
		</div>
		<div class="main-post__inner">
			<div class="main-post-content">
				<div class="main-post-full">
					<div class="rubric-posts-wrapper hide-desc">
						
						<?php foreach($arr as $item):?>
						
						<div class="post quote-item">
							<a class="img-wrapper quote-img myLinkModal myLinkModal<?php echo $item['id'];?>" href="#"><img src="<?php echo $item['imgsrc'];?>"/></a>
							<div class="post__content">
								<a class="myLinkModal<?php echo $item['id'];?>" href="#"><?php echo $item['name'];?></a>
								<div class="post-top"><span class="post-date"><?php echo $item['job'];?></span></div>
								
								<div id="myModal<?php echo $item['id'];?>" class="myModal">
									<div class="quote-content">
									  <div class="quote-modal-img" style="background-image: url(<?php echo $item['imgsrc'];?>);"></div>
									  <div class="quote-text">
										  <h2><?php echo $item['name'];?></h2>
										  <h4><?php echo $item['job'];?></h4>
										  <p><?php echo $item['quote'];?></p>
											<div class="share-box">
                        <span>поделиться:</span>
                        <div class="links-wrapper">
                            <a href="http://www.facebook.com/sharer.php?u=<?="https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>" target="_blank" class="img-wrapper fb"> <img width="7" height="14" src="<?php bloginfo('template_url'); ?>/assets/img/icons/fb-w.svg" alt="fb" /> </a>
                            <a href="http://vk.com/share.php?u=<?="https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>" target="_blank" class="img-wrapper wk"> <img width="17" height="10" src="<?php bloginfo('template_url'); ?>/assets/img/icons/wk-w.svg" alt="wk" /> </a>
                            <a href="https://t.me/share/url?url=<?="https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>" target="_blank" class="img-wrapper tg"> <img width="16" height="13" src="<?php bloginfo('template_url'); ?>/assets/img/icons/tg-w.svg" alt="tg" /> </a>
                        </div>
	                    </div>
										</div>
									</div>
								  <span id="myModal<?php echo $item['id'];?>__close" class="myModal__close close">ₓ</span>
								</div>
								<div id="myOverlay<?php echo $item['id'];?>" class="myOverlay"></div>
							</div>
						</div>	
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
