<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kapitaluz
 */

get_header();
?>

    <main>
        <section class="main-post">
            <div class="container">
                <div class="main-post__inner">
                    <div class="main-post__right">
                        <div class="main-post-content">
                            <div class="main-post-right">

                                <?php $args = array(
                                    'post_type' => 'post',
                                    'showposts' => '1',
                                    'offset' => '0',
                                    'category__not_in' => array(209, 219),
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'main',
                                            'field' => 'slug',
                                            'terms' => 'main',
                                        ),
                                    ),
                                );
                                $the_query = new WP_Query($args); ?>

                                <?php while ($the_query->have_posts()) :
                                $the_query->the_post(); ?>

                                <div class="img-wrapper main-post-img"><a href="<?php the_permalink(); ?>"><img
                                                src="<?php echo get_the_post_thumbnail_url($post, 'full'); ?>"
                                                alt="<?php the_title(); ?>"></a></div>
                                <h2 class="main-post-title"><a
                                            href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <span class="post-date">
									
								</span>
                            </div>
                            <?php endwhile; ?>

                            <div class="main-post-left">

                                <?php $args = array(
                                    'post_type' => 'post',
                                    'showposts' => '4',
                                    'offset' => '0',
                                    'category__not_in' => array(209, 219),
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'main',
                                            'operator' => 'NOT EXISTS'
                                        ),
                                    ),
                                );
                                $the_query = new WP_Query($args); ?>

                                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                                    <div class="related-post"><a
                                                href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <span
                                                class="post-date"><?php $time_diff = human_time_diff(get_post_time('U'), current_time('timestamp'));
                                            echo "$time_diff назад"; ?></span></div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="latest-news">
                            <div class="container">
                                <div class="lates-news__inner">
                                    <div class="big-post-wrapper" id="loadmoreContent">

                                        <?php $args = array(
                                            'post_type' => 'post',
                                            'showposts' => '4',
                                            'offset' => '5',
                                            'category__not_in' => array(209, 219),
                                            'tax_query' => array(
                                                array(
                                                    'category' => '',
                                                ),
                                            ),
                                        );

                                        $the_query = new WP_Query($args); ?>

                                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

                                            <div class="post">
                                                <a class="img-wrapper" href="<?php the_permalink(); ?>"> <img
                                                            src="<?php echo get_the_post_thumbnail_url($post, 'full'); ?>"
                                                            alt="<?php the_title(); ?>"> </a>
                                                <div class="post__content">
                                                    <div class="post-top"><?php echo get_the_category_list(); ?><span
                                                                class="breaker">/</span> <span
                                                                class="post-date"><?php echo get_the_date('j F Y'); ?></span>
                                                    </div>
                                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <?php
                                    ## global $wp_query;
                                    if ($the_query->max_num_pages > 1) {
                                        echo '<div class="misha_loadmore">Показать еще</div>'; // you can use <a> as well
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-post-ads">
                        <?php get_sidebar(); ?>
                        <div class="popular-posts">
                            <h4 class="sidesection-title">Популярное</h4>
                            <div class="popular-posts-wrapper">
                                <?php $args = array(
                                    'post_type' => 'post',
                                    'showposts' => '6',
                                    'offset' => '1',
                                    'category__not_in' => array(209, 219),
                                    'tax_query' => array(
                                        array(
                                            'category' => '',
                                        ),
                                    ),
                                );

                                $the_query = new WP_Query($args); ?>

                                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>


                                    <div class="post">

                                        <div class="post__content">
                                            <div class="post-top"><?php echo get_the_category_list(); ?><span
                                                        class="breaker">/</span> <span
                                                        class="post-date"><?php echo get_the_date('j F Y'); ?></span>
                                            </div>
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

<?php
get_footer();
