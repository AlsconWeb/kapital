<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kapitaluz
 */

?>
<footer id="footer">
    <div class="container">
            <div class="top">
                <a href="<?php echo get_home_url(); ?>" class="logo">
                    <img src="<?php echo get_template_directory_uri() . '/assets/img/logo.svg'; ?>" alt="logo"/>
                </a>
                <?php wp_nav_menu(array('theme_location' => 'menu-2')); ?>
            </div>
            <div class="bottom">
                <p>&copy; <?php echo date("Y"); ?>Kapital.uz | При использовании материалов гиперссылка на Kapital.uz
                    обязательна.</p>
            </div>
    </div>
</footer><!-- #footer -->

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-Y99T2EK1SC"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-Y99T2EK1SC');
</script>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</body>
</html>
