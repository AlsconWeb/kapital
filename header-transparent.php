<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kapitaluz
 */

$text_btn_one = get_field( 'text_btn_one', 'option' );
$link_btn_one = get_field( 'link_btn_one', 'option' );
$text_btn_two = get_field( 'text_btn_two', 'option' );
$link_btn_two = get_field( 'link_btn_two', 'option' );
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet"
		  href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700&amp;subset=cyrillic,cyrillic-ext,latin-ext">


	<script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>
	<meta name="google-site-verification" content="VoFlYTpgw7L-LpztcGpxyKc6NgskTopITYHT2ew1FF8"/>
	s
	<meta name="yandex-verification" content="d3a4143a9dee7516"/>


	<link rel="apple-touch-icon" href="/apple-touch-icon.png">
	<meta name="msapplication-TileColor" content="#c33">
	<meta name="msapplication-TileImage" content="/apple-touch-icon.png">
	<meta name="theme-color" content="#04144f">
	<link rel="manifest" href="/manifest.json">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header id="header">
	<div class="container">
		<div class="left">
			<a href="<?php echo get_home_url(); ?>" class="logo">
				<img src="<?php echo get_template_directory_uri() . '/assets/img/logo-white.svg'; ?>" alt="Site logo">
			</a>


			<?php
			wp_nav_menu(
				[
					'theme_location'  => 'menu-1',
					'container'       => 'nav',
					'container_class' => 'main-nav',
					'menu_class'      => 'main-menu',
					'items_wrap'      => '<ul class="%2$s">%3$s</ul>',

				]
			);
			?>
		</div>


		<div class="right">
			<?php if ( ! empty( $text_btn_one ) || ! empty( $link_btn_one ) ) : ?>
				<a
						class="button tbc-btn"
						id="btn-one"
						href="<?php echo esc_url( $link_btn_one ); ?>">
					<?php echo esc_html( $text_btn_one ); ?>
				</a>
			<?php endif; ?>
			<?php if ( ! empty( $text_btn_tow ) || ! empty( $link_btn_two ) ) : ?>
				<a
						class="button tbc-btn"
						id="btn-two"
						href="<?php echo esc_url( $link_btn_two ); ?>">
					<?php echo esc_html( $text_btn_two ); ?>
				</a>
			<?php endif; ?>
			<button class="js-searchbar-toggle searchbar-toggle-btn" type="button"></button>

			<!--            <a href="https://repost.uz/" target="_blank" class="repost"></a>-->
			<!--            <a href="https://t.me/KPTLUZ" target="_blank" class="tg"><span>Телеграм</span></a>-->

			<button type="button" class="js-burger-toggler burger-btn">
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>


		<?php
		wp_nav_menu(
			[
				'theme_location'  => 'menu-1',
				'container'       => 'nav',
				'container_class' => 'mob-nav js-mob-nav',
				'menu_class'      => 'main-menu',
				'items_wrap'      => '<ul class="%2$s">%3$s</ul>',

			]
		);
		?>

		<div class="search-form-wrapper js-search-form-wrapper">
			<form action="/">
				<label>
					<input type="text" name="s" required placeholder="Поиск..."/>
				</label>
				<button type="submit" class="search-btn img-wrapper"></button>
			</form>
		</div>
	</div>
</header><!-- #header -->


