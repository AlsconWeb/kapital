<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kapitaluz
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet"
		  href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700&amp;subset=cyrillic,cyrillic-ext,latin-ext">


	<script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>
	<meta name="google-site-verification" content="VoFlYTpgw7L-LpztcGpxyKc6NgskTopITYHT2ew1FF8"/>
	<meta name="yandex-verification" content="d3a4143a9dee7516"/>


	<link rel="apple-touch-icon" href="/apple-touch-icon.png">
	<meta name="msapplication-TileColor" content="#c33">
	<meta name="msapplication-TileImage" content="/apple-touch-icon.png">
	<meta name="theme-color" content="#04144f">
	<link rel="manifest" href="/manifest.json">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header class="main-header">
	<div class="container">
		<div class="main-header__inner">
			<a href="/" class="img-wrapper logo">
				<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/logo.svg" alt="Site logo">
			</a>

			<?php
			wp_nav_menu( [
				'theme_location'  => 'menu-1',
				'container'       => 'nav',
				'container_class' => 'main-nav',
				'menu_class'      => 'main-menu',
				'items_wrap'      => '<ul class="%2$s">%3$s</ul>',

			] );
			?>

			<div class="header-right">
				<button class="img-wrapper js-searchbar-toggle searchbar-toggle-btn" type="button">
					<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/icons/search-i.svg" class="search-img"
						 alt="search"/>
					<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/icons/cancel-i.svg" class="cancel-img"
						 alt="cancel"/>
				</button>
				<a href="https://repost.uz/" target="_blank" class="rpst"></a>
				<a href="https://t.me/KPTLUZ" target="_blank" class="tg-link-wrapper">
					<div class="img-wrapper">
						<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/icons/telegram.svg" alt="tg"/>
					</div>
					<span>Телеграм</span>
				</a>
				<button type="button" class="js-burger-toggler burger-btn img-wrapper">
					<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/icons/burger-i.svg" class="burger"
						 alt="burger"/>
					<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/icons/close-i.svg" class="close"
						 alt="close"/>
				</button>

			</div>
		</div>

		<?php
		wp_nav_menu( [
			'theme_location'  => 'menu-1',
			'container'       => 'nav',
			'container_class' => 'mob-nav js-mob-nav',
			'menu_class'      => 'main-menu',
			'items_wrap'      => '<ul class="%2$s">%3$s</ul>',

		] );
		?>

		<div class="search-form-wrapper js-search-form-wrapper">
			<form action="/">
				<label>
					<input type="text" name="s" required placeholder="Поиск..."/>
				</label>
				<button type="submit" class="search-btn img-wrapper">
					<img src="<?php bloginfo( 'template_url' ); ?>/assets/img/icons/search-i.svg" alt="search"/>
				</button>
			</form>
		</div>
	</div>
</header>


<div class="main-banner">
	<div class="container">
		<div class="img-wrapper">
			<div class="banner-adfox">

				<!--AdFox START-->
				<!--WebSail-Advertisement-->
				<!--Площадка: Kapital.uz / * / *-->
				<!--Тип баннера: 1150x100-->
				<!--Расположение: <верх страницы>-->
				<div id="adfox_160729078850626540"></div>
				<script>
					if ( typeof window.Ya !== 'undefined' ) {
						window.Ya.adfoxCode.createAdaptive( {
							ownerId: 277709,
							containerId: 'adfox_160729078850626540',
							params: {
								pp: 'g',
								ps: 'engg',
								p2: 'geya',
								puid1: ''
							}
						}, [ 'desktop', 'tablet' ], {
							tabletWidth: 768,
							phoneWidth: 576,
							isAutoReloads: false
						} );
					} else {
						console.error( 'header: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ' );
					}
				</script>

				<!--AdFox START-->
				<!--WebSail-Advertisement-->
				<!--Площадка: Kapital.uz / * / *-->
				<!--Тип баннера: 640x300-->
				<!--Расположение: <верх страницы>-->
				<div id="adfox_160728941561475980"></div>
				<script>
					if ( typeof window.Ya !== 'undefined' ) {
						window.Ya.adfoxCode.createAdaptive( {
							ownerId: 277709,
							containerId: 'adfox_160728941561475980',
							params: {
								pp: 'g',
								ps: 'engg',
								p2: 'geyc',
								puid1: ''
							}
						}, [ 'phone' ], {
							tabletWidth: 768,
							phoneWidth: 576,
							isAutoReloads: false
						} );
					} else {
						console.error( 'header: window.Ya.adfoxCode of undefined. Yandex Ad not loaded or XZ' );
					}
				</script>
			</div>
		</div>
	</div>
</div>
