<?php
/**
 * Created 11.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package kapitaluz
 */

get_header( 'biznes' );

get_template_part( 'template-parts/biznes', 'academya' );
?>
<div class="news">
	<div class="container">
		<?php
		if ( have_posts() ) :
			$i = 0;
			while ( have_posts() ) :
				the_post();
				$id = get_the_ID();
				if ( $i < 1 ) {
					echo '<div class="row row-cols-sm-2 row-cols-1">';
				}

				if ( 2 === $i ) {
					echo '</div><div class="row row-cols-xl-4 row-cols-lg-4 row-cols-md-2 row-cols-sm-2 row-cols-1" id="more-content">';
				}
				?>
				<div class="col">
					<div class="news-item">
						<?php
						if ( has_post_thumbnail( $id ) ) :
							the_post_thumbnail();
							?>
						<?php else : ?>
							<img src="//via.placeholder.com/561x348" alt="No Image">
						<?php endif; ?>
						<div class="description">
							<p class="data"><?php the_time( 'j M Y' ); ?></p>
							<h3 class="title"><?php the_title(); ?></h3>
							<p class="desc"><?php echo esc_html( get_the_excerpt( $id ) ); ?></p>
							<?php
							$terms = wp_get_post_terms( $id, 'category' );
							foreach ( $terms as $termPost ) :
								?>
								<a
										class="meta"
										href="<?php echo esc_url( get_term_link( $termPost->term_id ) ); ?>"
								>#<?php echo esc_html( $termPost->name ); ?></a>
							<?php endforeach; ?>
						</div>
						<a class="link" href="<?php the_permalink(); ?>"></a>
					</div>
				</div>
				<?php
				$i ++;
			endwhile;
			echo '</div>';
		endif;
		?>
		<div class="row">
			<div class="col-12">
				<a
						class="button read-more" href="#"
						data-all_post="<?php echo esc_attr( $termObj->count ); ?>"
						data-term_id="<?php echo esc_attr( $termObj->term_id ); ?>"
						data-nonce="<?php echo esc_attr( wp_create_nonce( 'iwp_load_more_' . $termObj->term_id ) ); ?>"
						data-page_num="1"
				>
					<?php esc_html_e( 'Больше новостей', 'kapitaluz' ); ?>
				</a>
			</div>
		</div>

	</div>
</div>
<?php get_footer( 'home' ); ?>
